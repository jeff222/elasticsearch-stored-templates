#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-studios
{
	"script": {
		"lang": "mustache",
		"source":

{
  "size": 10,
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "is_enabled": {
              "value": true,
              "boost": 1.0
            }
          }
        },
        {
          "wildcard": {
            "studio_title.keyword": {
              "wildcard": "*{{keyword}}*",
              "boost": 1.0
            }
          }
        }
      ],
      "adjust_pure_negative": true,
      "boost": 1.0
    }
  },
  "_source": {
    "includes": [
			"studio_id",
			"studio_title",
			"clip_count"
    ],
    "excludes": []
  },
  "sort": [
    {
      "_script": {
        "script": {
          "source": "InternalQlScriptUtils.nullSafeSortString(InternalQlScriptUtils.isNull(InternalQlScriptUtils.docValue(doc,params.v0)))",
          "lang": "painless",
          "params": {
            "v0": "month_popularity"
          }
        },
        "type": "string",
        "order": "asc"
      }
    },
    {
      "month_popularity": {
        "order": "desc",
        "missing": "_first",
        "unmapped_type": "long"
      }
    }
  ]
}

}}
EOF
