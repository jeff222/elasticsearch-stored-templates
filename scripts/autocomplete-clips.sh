#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-clips
{
	"script": {
		"lang": "mustache",
		"source":
{
	"query": {
		"bool": {
			"filter": {
				"bool": {
					"must": [
						{
							"term": {
								"is_enabled": true
							}
						},
						{
							"range": {
								"clip_timestamp": {
									"lt": "{{utc}}"
								}
							}
						}
					],
					"should": [
						{ "prefix": { "clip_title": "{{keyword}}" }},
						{ "fuzzy": { "clip_title": { "value": "{{keyword}}", "fuzziness": 1, "prefix_length": 0 }}}
					]
				}
			}
		}
	},
	"_source": ["clip_id", "producer_id", "clip_title"]
}}}
EOF
