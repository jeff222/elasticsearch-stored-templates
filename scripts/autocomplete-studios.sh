#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-studios
{
	"script": {
		"lang": "mustache",
		"source": {
			"size": 3,
			"query": {
				"bool": {
					"must": [
						{
							"multi_match": {
								"query": "{{keyword}}",
								"type": "phrase_prefix",
								"fields": [
									"studio_title"
								],
								"max_expansions": 10000,
								"lenient": true
							}
						}
					],
					"should": [
					],
					"filter": {
						"bool": {
							"must": [
								{
									"term": {
										"is_enabled": true
									}
								}
							]
						}
					}
				}
			},
			"_source": [
				"studio_id",
				"studio_title",
				"clip_count"
			]
		}
	}
}
EOF
