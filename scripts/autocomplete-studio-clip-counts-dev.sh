#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-studio-clip-counts
{
	"script": {
		"lang": "mustache",
		"source":


{
  "size": 0,
  "query": {
    "bool": {
      "must": [
        {
          "terms": {
            "producer_id": "{{producers}}",
             "boost": 1.0
          }
        },
        {
          "term": {
            "is_enabled": {
              "value": true,
              "boost": 1.0
            }
          }
        }
      ],
      "adjust_pure_negative": true,
      "boost": 1.0
    }
  },
  "_source": false,
  "stored_fields": "_none_",
  "aggregations": {
		"clips": {
			"terms": {
				"field": "producer_id"
			}
		}
  }
}



}}
EOF
