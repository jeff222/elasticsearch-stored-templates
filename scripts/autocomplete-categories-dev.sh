#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-categories-dev
{
	"script": {
		"lang": "mustache",
		"source":
{
	"query": {
		"bool": {
			"must": [
				{
					"term": {
						"is_enabled": {
							"value": true,
							"boost": 1.0
						}
					}
				},
				{
					"range": {
						"clip_timestamp": {
							"lte": "{{utc}}",
							"boost": 1.0
						}
					}
				}
			],
			"adjust_pure_negative": true,
			"boost": 1.0
		}
	},
	"track_total_hits": true,
	"size": 0,
	"_source": false,
	"stored_fields": "_none_",
	"aggregations": {
		"categories": {
			"terms": {
				"field": "clip_related_categories",
				"size": 10,
				"exclude": "",
				"order": {
					"_count": "desc"
				},
				"include": ".*{{keyword}}.*"
			},
			"aggs": {
				"type_count": {
					"cardinality": {
						"field": "producer_id"
					}
				},
				"top_clips_hits": {
					"top_hits": {
						"_source": {
							"includes": [
								"resolved_category",
								"clip_cat_int"
							]
						},
						"size": 100
					}
				}
			}
		}
	}
}
	}
}
EOF
