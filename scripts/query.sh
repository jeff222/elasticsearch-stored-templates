#!/bin/bash


upper=$(echo "$@" | awk '{print toupper($0)}')
cat <<EOF | http -vvv get localhost:19200/_msearch/template Content-type:application/x-ndjson
{ "index": "wlclipdata" }
{ "id": "autocomplete-categories", "params": {"utc": "1626840707","keyword": "$upper"}}
{ "index": "wlstudiodata" }
{ "id": "autocomplete-studios", "params": { "keyword": "$@" }}
{ "index": "wlclipdata" }
{ "id": "autocomplete-clips", "params": {"utc": "1626840707","keyword": "$@"}}
EOF



