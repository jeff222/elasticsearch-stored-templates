#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-categories
{
	"script": {
		"lang": "mustache",
		"source": {
			"query": {
				"bool": {
					"must": [
					],
					"should": [
					],
					"filter": {
						"bool": {
							"must": [
								{
									"term": {
										"is_enabled": true
									}
								},
								{
									"range": {
										"clip_timestamp": {
											"lt": "{{utc}}"
										}
									}
								}
							]
						}
					}
				}
			},
			"size": 0,
			"track_total_hits": true,
			"track_scores": false,
			"aggs": {
				"categories": {
					"terms": {
						"field": "resolved_category.keyword",
						"size": 10,
						"exclude": "",
						"order": {
							"_count": "desc"
						},
						"include": ".*{{keyword}}.*"
					},
					"aggs": {
						"top_clips_hits": {
							"top_hits": {
								"_source": {
									"includes": [
										"resolved_category",
										"clip_cat_int"
									]
								},
								"size": 1
							}
						}
					}
				}
			}
		}
	}
}
EOF
