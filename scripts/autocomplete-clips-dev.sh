#!/bin/bash

cat <<EOF | http -vv put localhost:19200/_scripts/autocomplete-clips-dev
{
	"script": {
		"lang": "mustache",
		"source":

{
  "query": {
    "bool": {
			"filter": {
				"bool": {
					"must": [
						{
							"term": {
								"is_enabled": true
							}
						},
						{
							"range": {
								"clip_timestamp": {
									"lt": "{{utc}}"
								}
							}
						}
					]
				}
			},
      "must": [
        {
          "match_phrase_prefix": {
            "clip_title": {
							"query": "{{keyword}}",
							"slop": 10
						}
          }
        }
      ]
    }
  },

  "_source": ["clip_id", "producer_id", "clip_title"],
  	"sort": [
		{
			"_script": {
				"script": {
					"source": "InternalQlScriptUtils.nullSafeSortString(InternalQlScriptUtils.isNull(InternalQlScriptUtils.docValue(doc,params.v0)))",
					"lang": "painless",
					"params": {
						"v0": "month_popularity"
					}
				},
				"type": "string",
				"order": "asc"
			}
		},
		{
			"month_popularity": {
				"order": "desc",
				"missing": "_first",
				"unmapped_type": "long"
			}
		}
	]

}

}}


EOF
