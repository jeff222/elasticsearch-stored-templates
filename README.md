# stored-templates

1. Add ssh configuration
2. ssh to rn (leave it running)
3. test your connection

Add SSH Config

~/.ssh/config

Host rn
User web1
HostName 10.30.115.162
ForwardAgent yes
LocalForward 19200 10.30.115.64:9200

CAUTION, THIS IS A PRODUCTION ELASTICSEARCH CLUSTER. Do not put,post,delete, etc unless you desire production results.

Connect to RN so we have LocalForward established

ssh rn

Test your setup

http://localhost:19200/\_cat/indices
